﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Matsko.Volodymyr.RobotChallange
{
    public class MatskoAlgorythm : IRobotAlgorithm
    {
        public string Author
        {
            get
            {
                return "Matsko Volodymyr";
            }
        }

        public string Description
        {
            get
            {
                return "Made by Matsko Volodymyr from PZ-33/2. It's robot algorithm";
            }
        }

        public int Variant
        {
            get
            {
                return 6;
            }
        }

        public bool InitialStep = true;
        public List<StantionsMap> StantionMap;
        public Dictionary<int, Position> StrategicPositions;

        public int Round;
        public int NumberOfRobots;
        public int MaxNumberOfRobots;


        public void Initialize(Map map, IList<Robot.Common.Robot> robots)
        {
            StantionMap = StantionsMap.Build(map);
            StrategicPositions = new Dictionary<int, Position>();
            InitialStep = false;
            Round = 0;
            NumberOfRobots = robots.Count;
            MaxNumberOfRobots = StantionMap.Count * 5;
            if (MaxNumberOfRobots > 100)
            {
                MaxNumberOfRobots = 100;
            }
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            Round = e.Number;
        }


        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            if (InitialStep)
            {
                Initialize(map, robots);
            }

            NumberOfRobots = robots.Count(x => x.Owner.Name == Author);

            if (Round == 0 || Round >= 48 || NumberOfRobots > MaxNumberOfRobots)
            {
                return NoSpawnSteps(robots, robotToMoveIndex, map);
            }
            else 
            {
                return WithSpawnSteps(robots, robotToMoveIndex, map);
            }
        }

        public RobotCommand NoSpawnSteps(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot activeRobot = robots[robotToMoveIndex];

            if (!StrategicPositions.ContainsKey(robotToMoveIndex))
            {
                Position baseStantion = MapHelper.FindNearestStantionThatNeedReinforcments(activeRobot, robots, StantionMap);
                StantionMap.Find(x => x.Position == baseStantion).NumberOfFriendlyRobots++;
                Position newPosition = MapHelper.FreePositionInArea(baseStantion, activeRobot, robots);
                StrategicPositions.Add(robotToMoveIndex, newPosition);
                return new MoveCommand() { NewPosition = StrategicPositions[robotToMoveIndex] };
            }

            if (StrategicPositions[robotToMoveIndex] != activeRobot.Position)
            {
                Position baseStantion = MapHelper.FindNearestStantionThatNeedReinforcments(activeRobot, robots, StantionMap);
                StantionMap.Find(x => x.Position == baseStantion).NumberOfFriendlyRobots++;
                Position newPosition = MapHelper.FreePositionInArea(baseStantion, activeRobot, robots);
                StrategicPositions[robotToMoveIndex] = newPosition;
                return new MoveCommand() { NewPosition = StrategicPositions[robotToMoveIndex] };
            }
            else if (map.Stations.Where(x => MapHelper.FindDistance(x.Position, activeRobot.Position) <= 2 && x.Energy > 0).Any())
            {
                return new CollectEnergyCommand();
            }
            else
            {
                return null;
            }
        }

        public RobotCommand WithSpawnSteps(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot activeRobot = robots[robotToMoveIndex];

            if (!StrategicPositions.ContainsKey(robotToMoveIndex))
            {
                Position baseStantion = MapHelper.FindNearestStantionThatNeedReinforcments(activeRobot, robots, StantionMap);
                StantionMap.Find(x => x.Position == baseStantion).NumberOfFriendlyRobots++;
                Position newPosition = MapHelper.FreePositionInArea(baseStantion, activeRobot, robots);
                StrategicPositions.Add(robotToMoveIndex, newPosition);
                return new MoveCommand() { NewPosition = StrategicPositions[robotToMoveIndex] };
            }

            Position nearestStantionThatNeedReinforcments = MapHelper.FindNearestStantionThatNeedReinforcments(activeRobot, robots, StantionMap);
            int distanceToNearestStantionThatNeedReinforcments = MapHelper.FindDistance(activeRobot.Position, nearestStantionThatNeedReinforcments);

            if (StrategicPositions[robotToMoveIndex] != activeRobot.Position && !StrategicPositions.ContainsValue(activeRobot.Position))
            {
                Position baseStantion = MapHelper.FindNearestStantionThatNeedReinforcments(activeRobot, robots, StantionMap);
                Position newPosition = MapHelper.FreePositionInArea(baseStantion, activeRobot, robots);
                StrategicPositions[robotToMoveIndex] = newPosition;
                return new MoveCommand() { NewPosition = StrategicPositions[robotToMoveIndex] };
            }
            else if (activeRobot.Energy > distanceToNearestStantionThatNeedReinforcments + 150
                && MapHelper.CanSpawn(activeRobot.Position, activeRobot, robots)
                && NumberOfRobots < MaxNumberOfRobots)
            {
                CreateNewRobotCommand create = new CreateNewRobotCommand();
                create.NewRobotEnergy = distanceToNearestStantionThatNeedReinforcments + 25;
                return create;
            }
            else if (map.Stations.Where(x => MapHelper.FindDistance(x.Position, activeRobot.Position) <= 2 && x.Energy > 0).Any())
            {
                return new CollectEnergyCommand();
            }
            else
            {
                return null;
            }
        }
    }
}
