﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Matsko.Volodymyr.RobotChallange
{
    public static class MapHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        public static bool IsCellFree(Position cell, Robot.Common.Robot activeRobot, IList<Robot.Common.Robot> robots)
        {
            return (robots.Where(x => x != activeRobot && x.Position == cell).Count() == 0);
        }

        public static Position FreePositionInArea(Position stantionPosition, Robot.Common.Robot activeRobot, IList<Robot.Common.Robot> robots)
        {
            if (IsCellFree(stantionPosition, activeRobot, robots))
            {
                return stantionPosition;
            }
            else
            {                
                for (int x = -2; x <= 2; x++)
                {
                    for (int y = -2; y <= 2; y++)
                    {
                        Position findingPosition = new Position(stantionPosition.X, stantionPosition.Y);
                        findingPosition.X += x;
                        findingPosition.Y += y;
                        if ((findingPosition.X >= 0 && findingPosition.Y >= 0)
                            && (findingPosition.X <= 100 && findingPosition.Y <= 100)
                            && IsCellFree(findingPosition, activeRobot, robots) 
                            && (FindDistance(findingPosition, stantionPosition) <= 2
                            && activeRobot.Energy >= FindDistance(activeRobot.Position, findingPosition)))
                        {
                            return findingPosition;
                        }
                    }
                }
            }
            return null;
        }

        public static bool CanSpawn(Position stantionPosition, Robot.Common.Robot activeRobot, IList<Robot.Common.Robot> robots)
        {
            for (int x = -2; x <= 2; x++)
            {
                for (int y = -2; y <= 2; y++)
                {
                    Position findingPosition = new Position(stantionPosition.X, stantionPosition.Y);
                    findingPosition.X += x;
                    findingPosition.Y += y;
                    if ((findingPosition.X >= 0 && findingPosition.Y >= 0)
                        && (findingPosition.X <= 100 && findingPosition.Y <= 100)
                        && IsCellFree(findingPosition, activeRobot, robots)
                        && findingPosition != activeRobot.Position)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static Position FindNearestStantionThatNeedReinforcments(Robot.Common.Robot activeRobot,
                IList<Robot.Common.Robot> robots, List<StantionsMap> stantionsMap)
        {
            Position nearest = null;
            int minDistance = 20000;
            foreach (var item in stantionsMap.Where(x => x.NeedReinforcments == true))
            {
                if (FreePositionInArea(item.Position, activeRobot, robots) != null
                    && stantionsMap.Where(x => x.Position == item.Position).First().NeedReinforcments)
                {
                    int d = FindDistance(item.Position, activeRobot.Position);
                    if (d < minDistance                         
                        && d < activeRobot.Energy)
                    {
                        minDistance = d;
                        nearest = item.Position;
                    }
                }
            }
            return nearest == null ? null : nearest;
        }
    }
}
