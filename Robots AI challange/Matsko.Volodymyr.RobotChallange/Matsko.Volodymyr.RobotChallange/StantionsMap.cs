﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Matsko.Volodymyr.RobotChallange
{
    public class StantionsMap
    {
        private Position position;
        public Position Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        private EnergyStation stantion;
        public EnergyStation Stantion
        {
            get
            {
                return stantion;
            }
            set
            {
                stantion = value;
            }
        }

        private int numberOfFriendlyRobots = 0;
        public int NumberOfFriendlyRobots
        {
            get
            {
                return numberOfFriendlyRobots;
            }
            set
            {
                numberOfFriendlyRobots = value;
            }
        }

        private const int MAX_ENERGY_PER_ROUND = 100;

        public bool NeedReinforcments
        {
            get
            {
                return ((numberOfFriendlyRobots * 40) < MAX_ENERGY_PER_ROUND);
            }            
        }

        public StantionsMap() { }

        public StantionsMap(Position position, EnergyStation stantion, int numberOfFriendlyRobots)
        {
            Position = position;
            Stantion = stantion;
            NumberOfFriendlyRobots = numberOfFriendlyRobots;
        }

        public static List<StantionsMap> Build(Map map)
        {
            List<StantionsMap> stantionsMap = new List<StantionsMap>();

            foreach (EnergyStation item in map.Stations)
            {
                StantionsMap stantion = new StantionsMap();
                stantion.Stantion = item;
                stantion.Position = item.Position;
                stantionsMap.Add(stantion);
            }

            return stantionsMap;
        }

        public override bool Equals(object obj)
        {
            StantionsMap other = obj as StantionsMap;
            if (other == null)
            {
                return false;
            }
            else
            {
                return (this.Position.Equals(other.Position)
                    && this.Stantion.Position.Equals(other.Stantion.Position)
                    && this.Stantion.Energy.Equals(other.Stantion.Energy)
                    && this.Stantion.RecoveryRate.Equals(other.Stantion.RecoveryRate)
                    && this.NumberOfFriendlyRobots.Equals(other.NumberOfFriendlyRobots));
            }
        }
    }
}
