﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;
using Matsko.Volodymyr.RobotChallange;
using NUnit.Framework;

namespace NUnitTests
{
    [TestFixture]
    public class TestStantionsMap
    {
        [TestCase(2,2,4,4)]
        public void TestStantionMapBuild(int aX, int aY, int bX, int bY)
        {
            //Arrange       
            EnergyStation stantion1 = new EnergyStation();
            stantion1.Position = new Position(aX, aY);

            EnergyStation stantion2 = new EnergyStation();
            stantion2.Position = new Position(bX, bY);

            Map map = new Map();
            map.Stations.Add(stantion1);
            map.Stations.Add(stantion2);
            
            List<StantionsMap> expected = new List<StantionsMap>();
            expected.Add(new StantionsMap());
            expected.Add(new StantionsMap());
            expected[0].Position = stantion1.Position;
            expected[0].Stantion = stantion1;
            expected[1].Position = stantion2.Position;
            expected[1].Stantion = stantion2;


            //Act
            List<StantionsMap> result = StantionsMap.Build(map);

            //Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestCase(2, 2, 3, 3, 3, 1, false)]
        [TestCase(2, 2, 3, 2, 2, 3, true)]
        public void TestEquals(int aX, int aY, int aNumberOfRobots, int bX, int bY, int bNumberOfRobots, bool expected)
        {
            //Arrange
            EnergyStation stantion1 = new EnergyStation();
            stantion1.Position = new Position(aX, aY);            

            EnergyStation stantion2 = new EnergyStation();
            stantion2.Position = new Position(bX, bY);

            StantionsMap stantions1 = new StantionsMap(stantion1.Position, stantion1, aNumberOfRobots);
            StantionsMap stantions2 = new StantionsMap(stantion2.Position, stantion2, bNumberOfRobots);

            //Act
            bool result = stantions1.Equals(stantions2);

            //Assert
            Assert.AreEqual(expected, result);
        }

    }
}
