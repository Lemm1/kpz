﻿using System;
using NUnit.Framework;
using Matsko.Volodymyr.RobotChallange;
using Robot.Common;
using System.Collections.Generic;
using System.Linq;

namespace NUnitTests
{
    [TestFixture]
    public class TestMapHelper
    {
        [TestCase(0, 0, 0, 0, 0)]
        [TestCase(0, 0, 0, 1, 1)]
        [TestCase(0, 0, 1, 1, 2)]
        [TestCase(0, 0, 100, 100, 20000)]
        public void TestFindDistance(int aX, int aY, int bX, int bY, int expected)
        {
            //Arrange
            Position a = new Position(aX, aY);
            Position b = new Position(bX, bY);

            //Act
            int result = MapHelper.FindDistance(a,b);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(1, 1, 2, 2, 1, 1, false)]
        [TestCase(1, 1, 2, 2, 3, 3, true)]
        public void TestIsCellFree(int cellX, int cellY, int activeRobotX, int activeRobotY
            , int enemyRobotX, int enemyRobotY, bool expected)
        {
            //Arrange
            Position cell = new Position(cellX, cellY);

            Robot.Common.Robot activeRobot = new Robot.Common.Robot();
            activeRobot.Position = new Position(activeRobotX, activeRobotY);
            Robot.Common.Robot enemyRobot = new Robot.Common.Robot();
            enemyRobot.Position = new Position(enemyRobotX, enemyRobotY);

            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(activeRobot);
            robots.Add(enemyRobot);
            
            //Act
            bool result = MapHelper.IsCellFree(cell, activeRobot, robots);

            //Assert
            Assert.AreEqual(expected,  result);
        }

        [TestCase(4, 4, 5, 5, 2, 3, 3, 3, 3)]
        [TestCase(2, 4, 5, 5, 10, 3, 3, 3, 3)]
        public void TestFindPositionInArea(int expectedA, int expectedB, int activeRobotX, int activeRobotY, int activeRobotEnergy
            , int enemyRobotX, int enemyRobotY, int stantionAX, int stantionAY)
        {
            //Arrange
            Position expected = new Position(expectedA, expectedB);

            Robot.Common.Robot activeRobot = new Robot.Common.Robot();
            activeRobot.Position = new Position(activeRobotX, activeRobotY);
            activeRobot.Energy = activeRobotEnergy;
            Robot.Common.Robot enemyRobot = new Robot.Common.Robot();
            enemyRobot.Position = new Position(enemyRobotX, enemyRobotY);

            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(activeRobot);
            robots.Add(enemyRobot);

            EnergyStation stantionA = new EnergyStation();
            stantionA.Position = new Position(stantionAX, stantionAY);

            //Act
            Position result = MapHelper.FreePositionInArea(stantionA.Position, activeRobot, robots);

            //Accert
            Assert.AreEqual(expected, result);
        }

        [TestCase(5, 5, 0, 0, 51, 3, 3, 3, 5, 5, 1)]
        [TestCase(3, 3, 5, 5, 10, 3, 3, 2, 0, 0, 3)]
        public void TestFindNearestStantionThatNeedReinforcments(int expectedA, int expectedB, int activeRobotX, int activeRobotY, int activeRobotEnergy
           , int stantionAX, int stantionAY, int stantionARobots, int stantionBX, int stantionBY, int stantionBRobots)
        {
            //Arrange
            Position expected = new Position(expectedA, expectedB);

            Robot.Common.Robot activeRobot = new Robot.Common.Robot();
            activeRobot.Position = new Position(activeRobotX, activeRobotY);
            activeRobot.Energy = activeRobotEnergy;

            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(activeRobot);

            EnergyStation stantionA = new EnergyStation();
            stantionA.Position = new Position(stantionAX, stantionAY);
            EnergyStation stantionB = new EnergyStation();
            stantionB.Position = new Position(stantionBX, stantionBY);

            List<StantionsMap> stantionsMap = new List<StantionsMap>();
            stantionsMap.Add(new StantionsMap());
            stantionsMap.Add(new StantionsMap());
            stantionsMap[0].Position = stantionA.Position;
            stantionsMap[0].Stantion = stantionA;
            stantionsMap[0].NumberOfFriendlyRobots = stantionARobots;
            stantionsMap[1].Position = stantionB.Position;
            stantionsMap[1].Stantion = stantionB;
            stantionsMap[1].NumberOfFriendlyRobots = stantionBRobots;

            //Act
            Position result = MapHelper.FindNearestStantionThatNeedReinforcments(activeRobot, robots, stantionsMap);

            //Assert
            Assert.AreEqual(expected, result);
        }


        [Test]
        public void TestCanSpawn__Positive()
        {
            //Arrange
            Robot.Common.Robot activeRobot = new Robot.Common.Robot();
            activeRobot.Position = new Position(1,1);

            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(activeRobot);

            //Act
            bool result = MapHelper.CanSpawn(activeRobot.Position, activeRobot, robots);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void TestCanSpawn__Negative()
        {
            //Arrange
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            for (int y = 0; y < 5; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    Robot.Common.Robot newRobot = new Robot.Common.Robot();
                    newRobot.Position = new Position(x, y);
                    robots.Add(newRobot);
                }               
            }

            //Act
            bool result = MapHelper.CanSpawn(robots[12].Position, robots[13], robots);

            //Assert
            Assert.IsFalse(result);
        }
    }
}
